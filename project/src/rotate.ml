(* Rotating an image by a given angle *)

let pi = 3.14159265
let degree_to_rad ang = (pi*.ang) /. 180.0
let rad_to_degree ang = (180.0*.ang) /. pi

let rotate tab ang =
    let w = Array.length tab and h = Array.length (tab.(0)) in
        let x_mid = (w-1)/2 and y_mid = (h-1)/2 in 
            let tab_out = Array.make_matrix w h 255 in
                for i = 0 to (w-1) do
                    for j = 0 to (h-1) do       
                    let x_new = x_mid + (i-x_mid)*(int_of_float(cos ang))
                        - (j-y_mid)*(int_of_float(sin ang)) 
                    and y_new = y_mid + (i-x_mid)*(int_of_float(sin ang))
                        - (j-y_mid)*(int_of_float(cos ang)) in
                    if (x_new>=0) && (x_new<=(w-1)) 
                        && (y_new>=0) && (y_new<=(h-1)) then
                    begin
                        tab_out.(x_new).(y_new) <- tab.(i).(j);
                    end;
                    done;
                done;
    tab_out
