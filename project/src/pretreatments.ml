(* Returns the sum of the list's elements  *)
let addition matsrc alist =
    let sum = ref 0 in
    begin
        for i = 0 to List.length alist do
        (
            sum := !sum + ((matsrc.(List.nth (List.nth  alist i) 0).(List.nth (List.nth  alist i) 1)) * (List.nth (List.nth  alist i) 3))
        )
        done;
        !sum
    end

(* Collisions management *)
let coordlist matsrc matftr x y collisioned =
    let ftrsize = Array.length matftr in
    let outputlist = ref [] in
    let increment_x = ref 0 in
        let increment_y = ref 0 in
            begin    
            for i = x - (ftrsize/2) to x + (ftrsize/2) do
            (
                increment_x := !increment_x + 1 ;

                for j = (y - (ftrsize/2)) to (y + (ftrsize/2)) do
                (
                    increment_y := !increment_y + 1 ;

                    if (collisioned) then
                    (
                        if ((i<0) ||
                            (j<0) ||
                            (i > Array.length matsrc) ||
                            (j > Array.length matsrc.(0))) 
                        then
                        outputlist := List.append !outputlist 
                                    [[i;j;matftr.(!increment_x).(!increment_y)]]
                    )
                     
                    else
                        outputlist := List.append !outputlist 
                                     [[i;j;matftr.(!increment_x).(!increment_y)]]
                )
                done
            )
            done;
                addition matsrc !outputlist
            end

(* Single treatment  *)
let superfct matsrc matftr x y =
    let ftrsize = Array.length matftr in
        begin
            (* No collisions*)
            if (
                ((x - ftrsize/2)>0)&&
                ((y - ftrsize/2)>0)&&
                ((Array.length matftr - ftrsize/2)>x)&&
                ((Array.length matftr.(0) - ftrsize/2)>y)
               ) 
            then
                coordlist matsrc matftr x y false
            (* Collisions Spotted*)
            else
                coordlist matsrc matftr x y true
        end

(* Processing of each pixel  *)
let process matsrc matftr filter  =
    let (x,y) = (Array.length matsrc , Array.length matsrc.(0)) in
        begin
            for i = 0 to x do
                for j = 0 to y do
                    matsrc.(i).(j) <- superfct matsrc matftr  i j
                done
            done    
        end

(* Filters  *)

let moyenne = Array.make 3 (Array.make 3 1)
