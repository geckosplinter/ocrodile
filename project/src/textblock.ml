(* Module Textblock *)

(* Text bloc structure *)
type text_block =
{
    mutable left_side: int;
    mutable top_side: int;
    mutable width: int;
    mutable height: int;
}

(* Module functions *)
(* h_interspace function returns the average number of pixels
    between two lettres in a line *)
let h_interspace matsrc begin_x end_x y =
    let inter = ref 0
    and nbr_inter = ref 0
    and last_char_x = ref 0
    and i = ref begin_x in
    while (!i < end_x) do
        if (matsrc.(!i).(y) = (1,1,1)) && (!i <> !last_char_x) then
            begin
                last_char_x  := !i;
                i := !i + 1;
                while (matsrc.(!i).(y) <> (0,0,0)) do
                    i := !i + 1
                done;
                inter := !inter + (!i - !last_char_x);
                nbr_inter := !nbr_inter + 1;
            end
    done;
    !inter / !nbr_inter

(* v_interspace function returns the average number of pixels
    between two lines *)
let v_interspace matsrc x begin_y end_y =
    let inter = ref 0
    and nbr_inter = ref 0
    and last_char_y = ref 0
    and j = ref begin_y in
    while (!j < end_y) do
        if (matsrc.(x).(!j) = (1,1,1)) && (!j <> !last_char_y) then
            begin
                last_char_y  := !j;
                j := !j + 1;
                while (matsrc.(x).(!j) <> (0,0,0)) do
                    j := !j + 1
                done;
                inter := !inter + (!j - !last_char_y);
                nbr_inter := !nbr_inter + 1;
            end
    done;
    !inter / !nbr_inter

(* is_line returns wether a specific interval is a line or not *)
let rec is_line matsrc begin_x end_x y =
    let bool_res = ref false
    and interspace = ref (h_interspace matsrc begin_x end_x y)
    and current_space = ref 0
    and i = ref begin_x in
    if (!i < end_x) then
        if (matsrc.(!i).(y) = (1,1,1)) then
                while (matsrc.(!i).(y) <> (1,1,1)) do
                    begin
                        current_space := !current_space + 1;
                        i := !i + 1;
                    end
                done;
                if (!current_space <= !interspace + 3) then
                    bool_res := (true && (is_line matsrc !i end_x y));
    !bool_res
    
(* returns the aproximate end of the current line *)
let end_line matsrc line_x line_y =
    begin
    let end_of_line = ref line_x
    and interspace = h_interspace matsrc line_x 
                                    ((Array.length matsrc) - 1) line_y 
    and current_inter = ref 0
    and i = ref line_x in
    while (!i < (Array.length matsrc.(0)) - 1) do
        if (matsrc.(!i).(line_y) = (1,1,1)) then
            begin
                i := !i + 1;
                while (matsrc.(!i).(line_y) <> (1,1,1))
                        && (!current_inter < interspace) do
                    i := !i + 1;
                    current_inter := !current_inter + 1;
                done;
                if (!current_inter > interspace + 5) then
                    end_of_line := !i;
            end
    done;
    !end_of_line
    end

(* returns the aproximate end of the current paragraph *)
let end_par matsrc line_x line_y =
    begin
    let end_of_par = ref line_y
    and interspace = ref (v_interspace matsrc line_x line_y
                                    ((Array.length matsrc.(line_x)) - 1))
    and current_inter = ref 0
    and j = ref line_y in
    while (!j < (Array.length (matsrc.(line_x)) - 1)) do
        if (matsrc.(line_x).(!j) = (1,1,1)) then
            begin
                j := !j + 1;
                while (matsrc.(line_x).(!j) <> (1,1,1))
                        && (!current_inter < !interspace) do
                    j := !j + 1;
                    current_inter := !current_inter + 1;
                done;
                if (!current_inter > !interspace + 5) then
                    end_of_par := !j;
            end
    done;
    !end_of_par;
    end

(* findblock function finds a textblock in the image
    matsrc represents the binarised image *)
let find_block matsrc =
    let text_block = { left_side = 0; top_side = 0;
                            width = 0; height = 0}
    and i = ref 0
    and j = ref 0 in
    while (!i < ((Array.length matsrc) - 1)) do
        while (!j < ((Array.length matsrc.(0))) - 1) do
            if matsrc.(!i).(!j) = (1,1,1) then (* pixel is black <=>
                                                beginning of a text
                                                block *)
                begin
                    if (is_line matsrc !i ((Array.length matsrc) - 1) !j) then
                        begin
                            text_block.left_side <- !i;
                            text_block.top_side <- !j;
                            text_block.width <- end_line matsrc !i !j;
                            text_block.height <- end_par matsrc !i !j;
                            j := !j + text_block.height;
                        end
                end
        done
    done;
    text_block

(* print_block function display a red rectangle
    which represents the text_block *)
let print_block matsrc block =
    begin
        for i = block.left_side to block.width do (* draw top line *)
            matsrc.(i).(block.top_side) <- (255,0,0)
        done;
        for j = (block.top_side + 1) to block.height do (* draw edges *)
            matsrc.(block.left_side).(j) <- (255,0,0);
            matsrc.(block.width).(j) <- (255,0,0)
        done;
        for i = block.left_side to block.width do (* draw bottom line *)
            matsrc.(i).(block.height) <- (255,0,0)
        done;
        matsrc
    end
