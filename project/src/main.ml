(* Text bloc structure *)
type text_column =
{
    mutable left: int;
    mutable right: int;
}

type text_line =
{
    mutable top: int;
    mutable bottom: int;
    mutable height: int;
    mutable columns_list : text_column list;
}

(*
    --------------------
    | TEXT RECOGNITION |
    --------------------
*)

(* check if a pixel at a y height of the matrix
    is a line or not (ie it has one black pixel) *)
let isLine matsrc y =
    let is_line = ref false
    and x = ref 0 in
    while (!x <= (Array.length matsrc) - 1)
            && (not !is_line) do
        is_line := !is_line || (matsrc.(!x).(y) = (0,0,0));
        x := !x + 1
    done;
    !is_line;;

(* check if there is a black pixel in a line (ie between top and bottom
    of a line) (so it's a colmun)
    returns true if there is a black pixel in the column *)
let isColumn matsrc line x =
    let y = ref line.top
    and is_column = ref false in
    while (!y <= line.bottom - 1)
            && (not !is_column) do
        is_column := !is_column || (matsrc.(x).(!y) = (0,0,0));
        y := !y + 1;
    done;
    !is_column;;

(* detect_columns function finds the columns (which delimit the characters)
    whitin a line *)
let detect_columns matsrc line =
    let column_list = ref []
    and x = ref 0 in
    while (!x <= (Array.length matsrc) - 1) do
        if (isColumn matsrc line !x) then
            begin
                let new_column = {left = !x; right = !x; } in
                x := !x + 1;
                while (!x <= (Array.length matsrc) - 1)
                        && (isColumn matsrc line !x) do
                    x := !x + 1
                done;
                new_column.right <- !x;
                column_list := List.append (new_column::[]) !column_list;
            end;
        x := !x + 1
    done;
    !column_list;;

(* detect_line finds the lines in the matrix
    and add them in a list of lines *)
let detect_line matsrc =
    let line_list = ref [] in
    begin
        let line_top = ref 0
        and i = ref 0
        and j = ref 0 in
        while (!i <= (Array.length matsrc) - 1) do
            while (!j <= (Array.length matsrc.(0)) - 1) do
                if (matsrc.(!i).(!j) = (0,0,0)) then
                    begin
                        let line = {top = 0; bottom = 0;
                                    height = 0; columns_list = []; } in
                        line_top := !j;
                        j := !j + 1;
                        while (!j < (Array.length matsrc.(0)) - 1)
                                &&(isLine matsrc !j) do
                            j := !j + 1;
                        done;
                        line.top <- !line_top;
                        line.bottom <- !j;
                        line.height <- !j - !line_top;
                        line.columns_list <- detect_columns matsrc line;
                        line_list := List.append (line::[]) !line_list;
                    end;
                j := !j + 1
            done;
            i := !i + 1
        done;
    end;
    !line_list;;

(* print_columns function will print the left and right
    borders of the columns found in a line *)
let rec print_columns matsrc columns_list line_top line_bottom =
    let myMatrix = ref matsrc in
    match columns_list with
    |[]-> !myMatrix
    |column::l ->
                begin

                    for j = line_top to line_bottom - 1 do
                        matsrc.(column.right - 1).(j) <- (0,255,0);
                        matsrc.(column.left).(j) <- (0,255,0)
                    done;
                    myMatrix := print_columns !myMatrix l
                                                line_top line_bottom
                end;
                !myMatrix;;

(* print_lines function will print the top and bottom
    borders of the text lines founded *)
let rec print_lines matsrc lines_list =
    let myMatrix = ref matsrc in
    match lines_list with
    |[]-> !myMatrix
    |line::l ->
                begin
                    for i = 0 to (Array.length matsrc) - 1 do
                        matsrc.(i).(line.top) <- (255,0,0);
                        matsrc.(i).(line.bottom) <- (255,0,0)
                    done;
                    myMatrix := print_columns matsrc line.columns_list
                                                line.top line.bottom;
                    myMatrix := print_lines !myMatrix l
                end;
                !myMatrix;;

(* Grayscale filtering  *)
let level (r,g,b) = (0.3 *. (float_of_int r)
            +. 0.59 *. (float_of_int g)
            +. 0.11 *. (float_of_int b))/. 255.

let blackNwhite (r,g,b) step =
    (*let step2 = ref step in
	if (!step2 > 0.8) then
	    begin
	    step2 :=  0.8
	    end;
	if (!step2 < 0.2) then
	    begin
	    step2 :=  0.2
	    end;
	if (level (r,g,b) > !step2) then (255,255,255)
	else (0,0,0)*)
    if (level (r,g,b) > step) then (255,255,255)
        else (0,0,0)

let averageImg matsrc =
        let nbr = ref 0. in
        for i = 0 to (Array.length matsrc) - 1 do
                for j = 0 to (Array.length matsrc.(0)) - 1 do
                    nbr := !nbr +. level(matsrc.(i).(j))
                done
        done;
        !nbr /. (float_of_int (Array.length matsrc) *. float_of_int
        (Array.length matsrc.(0)))

let toBlack matsrc  =
        let step = averageImg matsrc in
        for i = 0 to (Array.length matsrc) - 1 do
                for j = 0 to (Array.length matsrc.(0)) - 1 do
                     matsrc.(i).(j) <-  blackNwhite matsrc.(i).(j) step
                done
        done;
        matsrc

(* SDL initialisation *)

let sdl_init () =
    begin
        Sdl.init [`EVERYTHING];
        Sdlevent.enable_events Sdlevent.all_events_mask;
    end

(* Waiting for a key ... *)
let rec wait_key () =
    let e = Sdlevent.wait_event () in
        match e with
            Sdlevent.KEYDOWN _ -> ()
            | _ -> wait_key ()

(*
     *   show img dst
     *     Displays the img surface on
     *     destination surface dst
*)

let show img dst =
    let d = Sdlvideo.display_format img in
        Sdlvideo.blit_surface d dst ();
        Sdlvideo.flip dst

(* Image's dimensions *)

let get_dims img =
    ((Sdlvideo.surface_info img).Sdlvideo.w,
    (Sdlvideo.surface_info img).Sdlvideo.h)

(* tab2image produces an image from an array *)

let tab2image tab =
    let img =
    (Sdlvideo.create_RGB_surface [] (Array.length tab)
        (Array.length tab.(0))
        24 Int32.zero Int32.zero Int32.zero Int32.zero) in
            for i = 0 to (Array.length tab - 1) do
                for j = 0 to (Array.length tab.(i) - 1) do
                begin
                    Sdlvideo.put_pixel_color img i j tab.(i).(j)
                end;
                done;
            done;
    img

(* image2tab produces an array from an image *)

let image2tab img =
    let (width,height) = get_dims img in
        let tab = Array.make_matrix width height (0,0,0) in
            for i = 0 to width - 1 do
                for j = 0 to height - 1 do
                begin
                    tab.(i).(j) <- Sdlvideo.get_pixel_color img i j
                end;
                done;
            done;
    tab

(* Rotating an image by a given angle *)

let rotx x y center_x center_y angle =
        int_of_float ((cos angle) *. float (x - center_x)
            +. (sin angle) *. float (y - center_y) +. float (center_x))


(* HEAD
let averageImg matsrc =
        let nbr = 0. in   
        for i = 0 to matsrc.length do
        
                for j = 0 to matsrc.(0).length do
                
        nbr +. level(matsrc.(i).(j))
                done
        done
        nbr /. (float_of_int(i) *. float_of_int(j))



let blackNwhite (r,g,b) step  =
    if (level (r,g,b) > step  then (255,255,255)
    else (0,0,0)
=======*)
let roty x y center_x center_y angle =
        int_of_float (-.(sin angle) *. float (x - center_x)
            +. (cos angle) *. float (y - center_y) +. float (center_y))
(*>>>>>>> soutenance*)

let rotate mat angle =
    let angle = angle *. 3.14159265 /. 180. in
    let w = Array.length mat and
    h = Array.length mat.(0) in
    let dest_w = int_of_float (float w *. (abs_float (cos angle))
    +. float h *. (abs_float (sin angle))) in
    let dest_h = int_of_float (float h *. (abs_float (cos
    angle))
    +. float w *. (abs_float (sin angle))) in
    let dest_mat = Array.make_matrix dest_w dest_h (255,255,255) in
    for dest_x = 0 to dest_w - 1 do
        for dest_y = 0 to dest_h - 1 do
            let x = rotx dest_x
            dest_y (dest_w / 2)
            (dest_h / 2) angle and
            y = roty
            dest_x
            dest_y
            (dest_w / 2)
            (dest_h / 2)
            angle in
            if x >= 0 &&
            x < w && y
            >=0 && y < h
            then
                dest_mat.(dest_x).(dest_y)
                <-
                    mat.(x).(y);
        done
        done;
        dest_mat

(*
    -----------
    | FILTERS |
    -----------
*)

(* Returns the sum of the list's elements  *)
let addition matsrc alist =
  let a  = ref 0 in
  let b =  ref 0 in
  let c =  ref 0 in

    begin
      for i = 0 to (List.length alist) - 1 do
            (
	      let (x,y,z) = matsrc.(List.nth (List.nth alist i) 0).
              (List.nth (List.nth alist i) 1) in
                a := !a + x * (List.nth (List.nth alist i) 2);
		b := !b + y * (List.nth (List.nth alist i) 2);
		c := !c + z * (List.nth (List.nth alist i) 2);

            )
      done;
        (!a / 9,!b / 9,!c / 9)
    end

(* Collisions management *)
let coordlist matsrc matftr x y collisioned =
    let ftrsize = Array.length matftr in
    let outputlist = ref [] in
    let increment_x = ref 0 in
    let increment_y = ref 0 in
    begin
        for i = x - (ftrsize/2) to x + (ftrsize/2) do
            (

                for j = (y - (ftrsize/2)) to (y + (ftrsize/2)) do
                    (
                    if (!increment_x < 3) && (!increment_y < 3) then
                        (
                        if (collisioned) then
                            (
                                if not ((i<0) ||
                                (j<0) ||
                                (i > Array.length matsrc -1) ||
                                (j > Array.length matsrc.(0)-1))
                                then
                                (
                                outputlist := List.append !outputlist
                                [[i;j;1(*matftr.(!increment_x)
                                .(!increment_y)*)]];
                                ))

                            else
                            (
                            outputlist := List.append !outputlist
                            [[i;j;1(*matftr.(!increment_x)
                            .(!increment_y)*)]]
                            )
                            )
                    );
                        increment_y := !increment_y + 1 ;
                done;
                increment_x := !increment_x + 1 ;
                    )
         done;
                addition matsrc !outputlist
    end

    (* Single treatment  *)
let superfct matsrc matftr x y =
    let ftrsize = Array.length matftr in
    begin
        (* No collisions*)
        if (
            ((x - ftrsize/2)>0)&&
            ((y - ftrsize/2)>0)&&
            ((Array.length matsrc - ftrsize/2)>x)&&
            ((Array.length matsrc.(0) - ftrsize/2)>y)
            )
                                then
                                (
                                    coordlist matsrc matftr x y false)
         (* Collisions Spotted*)
        else
            (
                                    coordlist matsrc matftr x y true)
    end

    (* Processing of each pixel  *)
let process (matsrc : (int*int*int) array array) matftr =
    let (x,y) = (Array.length matsrc - 1, Array.length matsrc.(0) - 1) in
    begin
        for i = 0 to x do
            for j = 0 to y do
                matsrc.(i).(j) <- (superfct matsrc matftr i j)
        done
            done
    end

    (* Filters  *)

let moyenne = Array.make 3  (Array.make 3 1)

(* Average *)
let average matsrc x y =
        let s = ref 0 in
         begin
          for a = -1 to 1 do
           for b = -1 to 1 do
	     let (l,o,p) = matsrc.(x+a).(y+b)
               in s := !s + l
            done;
          done;
          let d = !s / 9 in
          (d,d,d);
          end
let call_average matsrc =
  let (x,y) = (Array.length matsrc - 1, Array.length matsrc.(0) - 1) in
  let matout = matsrc in
  begin
        for i = 1 to x-1 do
            for j = 1 to y-1 do
                matout.(i).(j) <- average matsrc i j
        done
        done;
matout
  end

(* main *)
    let main () =
    begin

(* We want 1 argument *)
if Array.length (Sys.argv) < 2 then
    failwith "Il manque le nom du fichier!";

    (* SDL Initialisation *)
    sdl_init ();

    (* Image Loading *)
    let img = Sdlloader.load_image Sys.argv.(1) in
    (* Getting dimensions *)
        let (w,h) = get_dims img in
        (* Creating display surface by doublebuffering *)
            let display =
                Sdlvideo.set_video_mode w h [`DOUBLEBUF] in
                (* Displaying image *)
                    show img display;

    wait_key (); (* Waiting for a key *)

    let mymatrix = toBlack (image2tab img) in
        show (tab2image mymatrix) display;

    wait_key ();

    let tab2 = rotate (toBlack (image2tab img)) 0.2 in
        show (tab2image tab2) display;

    wait_key ();

    let tab3 = call_average tab2 in
        show (tab2image tab3) display;

    wait_key ();

    let mymatrix2 = mymatrix in
        let myTextMatrix = print_lines mymatrix2 (detect_line (mymatrix2)) in
            show (tab2image myTextMatrix) display;


    wait_key ();

    exit 0;
    end

let _ = main ()
