(* tab2image produces an image from an array *)

let tab2image tab =
    let img = 
    (Sdlvideo.create_RGB_surface [] (Array.length tab)
        (Array.length tab.(0))
        24 Int32.zero Int32.zero Int32.zero Int32.zero) in
            for i = 0 to (Array.length tab - 1) do
                for j = 0 to (Array.length tab.(i) - 1) do
                begin
                    Sdlvideo.put_pixel_color img i j tab.(i).(j)
                end;
                done;
            done;
    img

(* image2tab produces an array from an image *)

let image2tab img =
    let (width,height) = get_dims img in
        let tab = Array.make_matrix width height (0,0,0) in
            for i = 0 to width - 1 do
                for j = 0 to height - 1 do
                begin
                    tab.(i).(j) <- Sdlvideo.get_pixel_color img i j
                end;
                done;
            done;
    tab
