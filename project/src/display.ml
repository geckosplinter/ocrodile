(* SDL initialisation *)

let sdl_init () =
    begin
        Sdl.init [`EVERYTHING];
        Sdlevent.enable_events Sdlevent.all_events_mask;
    end

(* Waiting for a key ... *)
let rec wait_key () =
    let e = Sdlevent.wait_event () in
        match e with
            Sdlevent.KEYDOWN _ -> ()
            | _ -> wait_key ()

(*
     *   show img dst
     *     Displays the img surface on
     *     destination surface dst
     *
*)

let show img dst =
    let d = Sdlvideo.display_format img in
        Sdlvideo.blit_surface d dst ();
        Sdlvideo.flip dst

(* Image's dimensions *)

let get_dims img =
    ((Sdlvideo.surface_info img).Sdlvideo.w,
    (Sdlvideo.surface_info img).Sdlvideo.h)
